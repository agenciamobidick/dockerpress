<?php
	echo shell_exec('
		sysvbanner dockerpress; \
		echo -e "\n"; \
		sysvbanner mobidick
	');

	$host = getenv("WORDPRESS_DB_HOST");
	$root = getenv("MYSQL_USER");
	$root_password = getenv("MYSQL_ROOT_PASSWORD");

	$user = getenv("WORDPRESS_DB_USER");
	$pass = getenv("WORDPRESS_DB_PASSWORD");
	$db = getenv("WORDPRESS_DB_NAME");

	$bd_existe = false;
	$user_existe = false;
	$user_existe_frase = 'O usuário '.$user.' ainda NÃO está em uso';

	try {
		$dbh = new PDO("mysql:host=$host", $root, $root_password);

		// checa se o banco existe
		if($dbh->query("use ".$db) == true){
			echo shell_exec('echo O banco de dados '.$db.' já está em uso');
			$bd_existe = true;
		}else{
			echo shell_exec('echo O banco de dados '.$db.' ainda NÃO está em uso');
		}

		// checar se o user do banco existe
		$sql = "SELECT User FROM mysql.user";
		$sql2 = $dbh->query($sql);
		foreach ($sql2 as $row) {
			if($row['User'] == $user){
				$user_existe = true;
				$user_existe_frase = 'O usuário '.$user.' já está em uso';
				break;
			}
		}

		echo shell_exec('echo '.$user_existe_frase);

		// se o user e banco não existem ele cria ambos
		if($bd_existe == false && $user_existe == false){
			$dbh->exec("CREATE DATABASE `$db`;")
			or die(print_r($dbh->errorInfo(), true));

			$dbh->exec("CREATE USER '$user'@'%' IDENTIFIED BY '$pass';
						GRANT ALL ON `$db`.* TO '$user'@'%';
						FLUSH PRIVILEGES;");
			echo shell_exec('echo O usuário '.$user.' e banco de dados '.$db.' foram criados com sucesso');
		}else{
			echo shell_exec('echo O usuário '.$user.' e banco de dados '.$db.' não foram criados pois já estão em uso');
		}

	}catch (PDOException $e) {
		die("DB ERROR: " . $e->getMessage());
	}

?>